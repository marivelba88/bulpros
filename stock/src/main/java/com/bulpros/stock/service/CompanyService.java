package com.bulpros.stock.service;

import com.bulpros.stock.configuration.AppConfig;
import com.bulpros.stock.dto.CompanyDto;
import com.bulpros.stock.dto.LogoDto;
import com.bulpros.stock.model.Company;
import com.bulpros.stock.repository.CompanyRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository repository;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ModelMapper modelMapper;

    public Company getCompanyBySymbol(String symbol) {

        URI targetUrl = UriComponentsBuilder.fromUriString(AppConfig.COMPANY_URL)  // Build the base link
                .path(symbol + "/company")
                .queryParam("token", AppConfig.TOKEN)
                .build()
                .encode()
                .toUri();
        CompanyDto response = restTemplate.getForObject(
                targetUrl,
                CompanyDto.class);

        String logo = getCompanyLogo(symbol);
        Optional<Company> company = repository.findBySymbol(symbol);
        if (!company.isPresent()) {
            return repository.saveAndFlush(convertToEntity(response, logo));
        }
        return company.get();
    }

    public String getCompanyLogo(String symbol) {

        URI targetUrl = UriComponentsBuilder.fromUriString(AppConfig.COMPANY_URL)  // Build the base link
                .path(symbol + "/logo")
                .queryParam("token", AppConfig.TOKEN)
                .build()
                .encode()
                .toUri();
        LogoDto response = restTemplate.getForObject(
                targetUrl,
                LogoDto.class);
        return response.getUrl() != "0"? response.getUrl() : "";
    }

    private Company convertToEntity(CompanyDto companyDto, String logo) {
        Company company = modelMapper.map(companyDto, Company.class);
        company.setLogo(logo);
        return company;
    }

}