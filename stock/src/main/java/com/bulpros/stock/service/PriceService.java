package com.bulpros.stock.service;

import com.bulpros.stock.model.Company;
import com.bulpros.stock.model.Price;
import com.bulpros.stock.repository.CompanyRepository;
import com.bulpros.stock.repository.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static com.bulpros.stock.configuration.AppConfig.*;

@Service
public class PriceService {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private PriceRepository priceRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private RestTemplate restTemplate;

    public void getPriceByCompanySymbols(List<String> symbols) {
        String symbolsParam = String.join(",", symbols);
        URI targetUrl = UriComponentsBuilder.fromUriString(API_URL)  // Build the base link
                .path("/price")
                .queryParam("symbols", symbolsParam)
                .queryParam("token", TOKEN)
                .build()
                .encode()
                .toUri();

        Float[] response = restTemplate.getForObject(targetUrl, Float[].class);
        for (int i = 0; i < symbols.size(); i++) {
            Optional<Company> optionalCompany = companyRepository.findBySymbol(symbols.get(i));
            Company company = optionalCompany.isPresent() ?
                    optionalCompany.get() : companyService.getCompanyBySymbol(symbols.get(i));
            Price price = convertToEntity(response[i], company);
            priceRepository.saveAndFlush(price);
        }
    }

    private Price convertToEntity(Float price, Company company) {
        Price result = new Price();
        result.setPrice(price);
        result.setCompany(company);
        return result;
    }
}
