package com.bulpros.stock.repository;

import com.bulpros.stock.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {
    Optional<List<Price>> findAllByCompany_Id(Long companyId);
}
