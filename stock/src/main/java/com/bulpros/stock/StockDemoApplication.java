package com.bulpros.stock;

import com.bulpros.stock.service.PriceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;

import java.util.*;

@SpringBootApplication
@EnableJpaAuditing
@EnableScheduling
public class StockDemoApplication {
    @Autowired
    PriceService priceService;

    List<String> companies = new ArrayList<>();

    public static void main(String[] args) {
        SpringApplication.run(StockDemoApplication.class, args);
    }

    public void getJsonData(){
        JSONParser parser = new JSONParser();
        File file = new File(getClass().getClassLoader().getResource("Configurations.json").getFile());
        if (file == null) return;
        try {
            Object obj = parser.parse(new FileReader(file));

            JSONObject jsonObject = (JSONObject) obj;

            JSONArray companyList = (JSONArray) jsonObject.get("Company List");
            companyList.forEach(company-> companies.add((String) company));
            companies.forEach(elm->System.out.println(elm));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "${cron.expression}")
    public void scheduleFixedRateWithInitialDelayTask() {
        if(Objects.nonNull(companies)){
            priceService.getPriceByCompanySymbols(companies);
        }
    }

    @Bean
    public CommandLineRunner run() throws Exception {
        return args -> {
            getJsonData();
            scheduleFixedRateWithInitialDelayTask();
        };
    }

}

