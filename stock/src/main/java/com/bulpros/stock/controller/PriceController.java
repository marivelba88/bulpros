package com.bulpros.stock.controller;

import com.bulpros.stock.dto.StockDto;
import com.bulpros.stock.exception.BadRequestException;
import com.bulpros.stock.exception.ResourceNotFoundException;
import com.bulpros.stock.model.Company;
import com.bulpros.stock.repository.CompanyRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class PriceController {
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CompanyRepository companyRepository;

    @GetMapping("/stock/prices/{symbols}")
    public List<StockDto> getAllPricesByCompanyId(@PathVariable List<String> symbols,
                                                  @RequestParam(required = false) String dateFrom,
                                                  @RequestParam(required = false) String dateTo) throws ParseException {
        List<StockDto> stockDtos = new ArrayList<>();
        symbols.forEach(symbol -> {
            Optional<Company> companyOp = Optional.ofNullable(companyRepository.findBySymbol(symbol)
                    .orElseThrow(() -> new ResourceNotFoundException("Company " + symbol + " not found")));
            stockDtos.add(convertToDto(companyOp.get()));
        });

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        if (dateFrom != null) {
            Date from = df.parse(dateFrom);
            Date to = dateTo != null ? df.parse(dateTo) : new Date();
            if (from.after(to)) {
                throw new BadRequestException("The dates are invalid");
            }
            stockDtos.forEach(stockDto -> stockDto.setPrices(stockDto.getPrices().stream()
                    .filter(price -> (price.getCreatedAt().after(from)
                            && price.getCreatedAt().before(to)))
                    .collect(Collectors.toList())));

        }
        return stockDtos;
    }

    public StockDto convertToDto(Company company) {
        return modelMapper.map(company, StockDto.class);
    }

}
