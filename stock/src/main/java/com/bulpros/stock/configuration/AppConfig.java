package com.bulpros.stock.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {
    static final public String API_URL = "https://sandbox.iexapis.com/stable/stock/market";
    static final public String COMPANY_URL = "https://sandbox.iexapis.com/stable/stock/";
    static final public String TOKEN = "Tpk_72fda3381ba547489eb6ad3bf3ed7fe3";

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}