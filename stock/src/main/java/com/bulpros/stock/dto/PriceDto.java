package com.bulpros.stock.dto;

import java.util.Date;

public class PriceDto {
    private Float price;
    private Date createdAt;

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

}
