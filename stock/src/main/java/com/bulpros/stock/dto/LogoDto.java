package com.bulpros.stock.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LogoDto {
    private String url;

    public String getUrl() {
        return url;
    }

}
