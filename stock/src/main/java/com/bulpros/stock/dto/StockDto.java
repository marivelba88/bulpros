package com.bulpros.stock.dto;

import java.util.List;

public class StockDto {
    private String symbol;

    private String companyName;

    private String logo;

    private List<PriceDto> prices;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<PriceDto> getPrices() {
        return prices;
    }

    public void setPrices(List<PriceDto> prices) {
        this.prices = prices;
    }
}
